﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DierenasielBreda.Core.DomainModel;
using DierenasielBreda.Core.DomainServices;
using Microsoft.EntityFrameworkCore;

namespace DierenasielBreda.DB
{
    public class EFKlantRepository : IKlantRepository
    {
        private ApplicationDbContext context;

        public EFKlantRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public Klant AddKlant(Klant klant)
        {
            // Klant toevoegen
            context.Klant.Add(klant);
            context.SaveChanges();

            return klant;
        }

        public Klant DeleteKlant(int klantId)
        {
            // Controleer of klant bestaat
            Klant dbEntry = context.Klant.Find(klantId);

            // Klant gevonden? Verwijderen.
            if (dbEntry != null)
            {
                context.Klant.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }

        public IQueryable<Klant> GetAllKlanten()
        {
            // Return alle klanten
            return context.Klant;
        }

        public Klant GetKlant(int klantId)
        {
            // Zoekt naar de klant op Id
            Klant dbEntry = context.Klant.Find(klantId);

            // return klant of null
            return dbEntry;
        }

        public void UpdateKlant(Klant klant)
        {
            // Zoekt naar het dier
            Klant dbEntry = context.Klant.Find(klant.Id);

            // Bestaat het dier? Neem de waardes over van het meegestuurde dier
            if (dbEntry != null)
            {
                dbEntry.Voornaam = klant.Voornaam;
                dbEntry.Tussenvoegsel = klant.Tussenvoegsel;
                dbEntry.Achternaam = klant.Achternaam;
                dbEntry.Email = klant.Email;
                dbEntry.GeboorteDatum = klant.GeboorteDatum;
                dbEntry.TelefoonNummer = klant.TelefoonNummer;
                dbEntry.Straatnaam = klant.Straatnaam;
                dbEntry.Huisnummer = klant.Huisnummer;
                dbEntry.HuisnummerToevoeging = klant.HuisnummerToevoeging;
                dbEntry.Postcode = klant.Postcode;
                dbEntry.Plaatsnaam = klant.Plaatsnaam;
                dbEntry.Plaatsnaam = klant.Plaatsnaam;

                context.SaveChanges();
            }
        }
    }
}
