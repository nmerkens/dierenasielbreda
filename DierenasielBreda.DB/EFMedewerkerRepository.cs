﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using DierenasielBreda.Core.DomainModel;
using DierenasielBreda.Core.DomainServices;

namespace DierenasielBreda.DB
{
    public class EFMedewerkerRepository : IMedewerkerRepository
    {
        private ApplicationDbContext context;

        public EFMedewerkerRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public Medewerker AddMedewerker(Medewerker medewerker)
        {
            context.Medewerker.Add(medewerker);
            context.SaveChanges();
            return medewerker;
        }

        public void DeleteMedewerker(int medewerkerId)
        {
            //zoekt medewerker op ID als medewerker niet gevonden is returned NULL.
            Medewerker dbEntry = context.Medewerker.Find(medewerkerId);
            //checkt of return waarde dbEntry niet NULL is.
            if (dbEntry != null)
            {
                //Verwijdert de medewerker
                context.Medewerker.Remove(dbEntry);
                context.SaveChanges();
            }
        }

        public IQueryable<Medewerker> GetAllMedewerker()
        {
            return context.Medewerker;
        }

        public Medewerker GetMedewerker(int medewerkerId)
        {
            Medewerker dbEntry = context.Medewerker.Find(medewerkerId);
            return dbEntry;
        }

        public void UpdateMedewerker(Medewerker medewerker)
        {
            Medewerker dbEntry = context.Medewerker.Find(medewerker.Id);
            if (dbEntry != null)
            {
                dbEntry.Voornaam = medewerker.Voornaam;
                dbEntry.Tussenvoegsel = medewerker.Tussenvoegsel;
                dbEntry.Achternaam = medewerker.Achternaam;
                dbEntry.Email = medewerker.Email;
                dbEntry.TelefoonNummer = medewerker.TelefoonNummer;
                dbEntry.GeboorteDatum = medewerker.GeboorteDatum;
                dbEntry.Wachtwoord = medewerker.Wachtwoord;
                dbEntry.IsVrijwilliger = medewerker.IsVrijwilliger;

                context.SaveChanges();
            }
        }
    }
}
