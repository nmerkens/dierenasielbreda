﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

using DierenasielBreda.Core.DomainModel;
using System.Data.Common;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace DierenasielBreda.DB
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options) { }

        public DbSet<Behandeling> Behandeling { get; set; }
        public DbSet<Dier> Dier { get; set; }
        public DbSet<Klant> Klant { get; set; }
        public DbSet<Medewerker> Medewerker { get; set; }
        public DbSet<Opmerking> Opmerking { get; set; }
        public DbSet<VacatureDier> VacatureDier { get; set; }
        public DbSet<Verblijf> Verblijf { get; set; }
        

    }
}
