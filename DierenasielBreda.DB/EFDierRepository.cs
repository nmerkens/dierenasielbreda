﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DierenasielBreda.Core.DomainModel;
using DierenasielBreda.Core.DomainServices;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DierenasielBreda.DB
{
    public class EFDierRepository : IDierRepository
    {
        private ApplicationDbContext context;

        public EFDierRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public Dier AddDier(Dier dier)
        {
            // insert dier
            context.Dier.Add(dier);
            context.SaveChangesAsync();

            return dier;
        }

        public async Task<Dier> AddDierAsync(Dier dier)
        {
            await context.Dier.AddAsync(dier);
            await context.SaveChangesAsync();

            return dier;
        }



        public Dier DeleteDier(int dierId)
        {
            // Controleer of dier bestaat
            Dier dbEntry = context.Dier.Find(dierId);

            // Dier gevonden? Verwijderen.
            if (dbEntry != null)
            {
                context.Dier.Remove(dbEntry);
                context.SaveChanges();
            }

            return dbEntry;
        }

        public IQueryable<Dier> GetDieren()
        {
            // Return alle dieren
            return context.Dier;
        }


        public async Task<IEnumerable<Dier>> GetDierenAsync()
        {
            return await GetDieren()
                .OrderBy(d => d.Naam)
                .ToListAsync();
        }

        public Dier GetDier(int dierId)
        {
            // Zoekt naar het dier op ID
            Dier dbEntry = context.Dier.Find(dierId);

            // return dier of null
            return dbEntry;
        }

        public void UpdateDier(Dier dier)
        {
            // Zoekt naar het dier
            Dier dbEntry = context.Dier.Find(dier.Id);

            // Bestaat het dier? Neem de waardes over van het meegestuurde dier
            if (dbEntry != null)
            {
                dbEntry.Naam = dier.Naam;
                dbEntry.Geboortedatum = dier.Geboortedatum;
                dbEntry.Leeftijd = dier.Leeftijd;
                dbEntry.GeschatteLeeftijd = dier.GeschatteLeeftijd;
                dbEntry.Omschrijving = dier.Omschrijving;
                dbEntry.Diersoort = dier.Diersoort;
                dbEntry.Ras = dier.Ras;
                dbEntry.Geslacht = dier.Geslacht;
                dbEntry.Foto = dier.Foto;
                dbEntry.DatumBinnenkomst = dier.DatumBinnenkomst;
                dbEntry.DatumAdoptie = dier.DatumAdoptie;
                dbEntry.DatumOverlijden = dier.DatumOverlijden;
                dbEntry.Gecastreerd = dier.Gecastreerd;
                dbEntry.KanMetKinderen = dier.KanMetKinderen;
                dbEntry.Opmerking = dier.Opmerking;
                dbEntry.RedenAfstand = dier.RedenAfstand;
                dbEntry.Adopteerbaar = dier.Adopteerbaar;

                context.SaveChanges();
            }
        }

    }
}
