﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DierenasielBreda.Core.DomainModel;
using DierenasielBreda.Core.DomainServices;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DierenasielBreda.DB
{
    public class EFVacatureDierRepository : IVacatureDierRepository
    {
        private ApplicationDbContext context;

        public EFVacatureDierRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public async Task<VacatureDier> AddVacatureDierAsync(VacatureDier vDier)
        {
            await context.VacatureDier.AddAsync(vDier);
            await context.SaveChangesAsync();

            return vDier;
        }

        public VacatureDier DeleteVacatureDier(int vDierId)
        {
            VacatureDier dbEntry = context.VacatureDier.Find(vDierId);

            // Dier gevonden? Verwijderen.
            if (dbEntry != null)
            {
                context.VacatureDier.Remove(dbEntry);
                context.SaveChanges();
            }

            return dbEntry;
        }

        public VacatureDier GetVacatureDier(int vDierId)
        {
            VacatureDier dbEntry = context.VacatureDier.Find(vDierId);

            // return dier of null
            return dbEntry;
        }

        public IQueryable<VacatureDier> GetVacatureDieren()
        {
            return context.VacatureDier;
        }

        public async Task<IEnumerable<VacatureDier>> GetVacatureDierenAsync()
        {
            return await GetVacatureDieren()
                .OrderBy(d => d.Naam)
                .ToListAsync(); 
        }

        public void UpdateDier(VacatureDier vDier)
        {
            throw new NotImplementedException();
        }
    }
}
