﻿using DierenasielBreda.Core.DomainModel;
using DierenasielBreda.Core.DomainServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DierenasielBreda.DB
{
    public class EFVerblijfRepository : IVerblijfRepository
    {
        private ApplicationDbContext context;

        public EFVerblijfRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }
        public Verblijf GetVerblijf(int verblijfId)
        {
            Verblijf dbEntry = context.Verblijf.Find(verblijfId);

            // return dier of null
            return dbEntry;
        }

        public IQueryable<Verblijf> GetVerblijven()
        {
            return context.Verblijf;
        }

        public async Task<IEnumerable<Verblijf>> GetverblijvenAsync()
        {
            return await GetVerblijven()
                .OrderBy(d => d.Naam)
                .ToListAsync();
        }
    }
}
