﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DierenasielBreda.Core.DomainModel;
using DierenasielBreda.Core.DomainServices;
using DierenasielBreda.DB;

namespace DierenasielBreda.DB
{
    public class EFBehandelingRepository : IBehandelingRepository
    {
        private ApplicationDbContext context;

        public EFBehandelingRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public Behandeling AddBehandeling(Behandeling behandeling)
        {
            context.Behandeling.Add(behandeling);
            context.SaveChanges();
            return behandeling;
        }

        public void DeleteBehandeling(int behandelingId)
        {
            //zoekt Behandeling op ID als Behandeling niet gevonden is returned NULL.
            Behandeling dbEntry = context.Behandeling.Find(behandelingId);
            //checkt of return waarde dbEntry niet NULL is.
            if (dbEntry != null)
            {
                //Verwijdert de behandeling
                context.Behandeling.Remove(dbEntry);
                context.SaveChanges();
            }
        }

        public IQueryable<Behandeling> GetAllBehandeling()
        {
            //returned alle behandelingen.
            return context.Behandeling;
        }

        public Behandeling GetBehandeling(int behandelingId)
        {
            //zoekt behandeling op ID als behandeling niet gevonden is returned NULL.
            Behandeling dbEntry = context.Behandeling.Find(behandelingId);
            return dbEntry;
        }

        public void UpdateBehandeling(Behandeling behandeling)
        {
            //zoekt dier op ID als dier niet gevonden is returned NULL.
            Behandeling dbEntry = context.Behandeling.Find(behandeling.Id);
            //checkt of return waarde dbEntry niet NULL is.
            if (dbEntry != null)
            {
                dbEntry.TypeBehandeling = behandeling.TypeBehandeling;
                dbEntry.Omschrijving = behandeling.Omschrijving;
                dbEntry.Kosten = behandeling.Kosten;
                dbEntry.MagVanafLeeftijd = behandeling.MagVanafLeeftijd;
                dbEntry.NaamBehandelaar = behandeling.NaamBehandelaar;
                dbEntry.DatumBehandeling = behandeling.DatumBehandeling;

                context.SaveChanges();
            }
        }
    }
}
