﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DierenasielBreda.DB.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Klant",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Voornaam = table.Column<string>(nullable: false),
                    Tussenvoegsel = table.Column<string>(nullable: true),
                    Achternaam = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    TelefoonNummer = table.Column<int>(nullable: false),
                    GeboorteDatum = table.Column<DateTime>(nullable: false),
                    Straatnaam = table.Column<string>(nullable: false),
                    Huisnummer = table.Column<int>(nullable: false),
                    HuisnummerToevoeging = table.Column<string>(nullable: true),
                    Postcode = table.Column<string>(nullable: false),
                    Plaatsnaam = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Klant", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Medewerker",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Voornaam = table.Column<string>(nullable: false),
                    Tussenvoegsel = table.Column<string>(nullable: true),
                    Achternaam = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    TelefoonNummer = table.Column<int>(nullable: false),
                    GeboorteDatum = table.Column<DateTime>(nullable: false),
                    Wachtwoord = table.Column<string>(nullable: false),
                    IsVrijwilliger = table.Column<bool>(nullable: false),
                    OpmerkingID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Medewerker", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VacatureDier",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Naam = table.Column<string>(nullable: false),
                    Diersoort = table.Column<string>(nullable: false),
                    Geslacht = table.Column<bool>(nullable: false),
                    Gecastreerd = table.Column<bool>(nullable: false),
                    RedenAfstand = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VacatureDier", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Verblijf",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Naam = table.Column<string>(nullable: true),
                    Capaciteit = table.Column<int>(nullable: false),
                    Diersoort = table.Column<string>(nullable: true),
                    Geslacht = table.Column<bool>(nullable: true),
                    Castratie = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Verblijf", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Dier",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Naam = table.Column<string>(nullable: false),
                    Geboortedatum = table.Column<DateTime>(nullable: false),
                    Leeftijd = table.Column<int>(nullable: false),
                    GeschatteLeeftijd = table.Column<int>(nullable: false),
                    Omschrijving = table.Column<string>(nullable: false),
                    Diersoort = table.Column<string>(nullable: false),
                    Ras = table.Column<string>(nullable: true),
                    Geslacht = table.Column<bool>(nullable: false),
                    Foto = table.Column<string>(nullable: true),
                    DatumBinnenkomst = table.Column<DateTime>(nullable: false),
                    DatumAdoptie = table.Column<DateTime>(nullable: true),
                    DatumOverlijden = table.Column<DateTime>(nullable: true),
                    Gecastreerd = table.Column<bool>(nullable: false),
                    KanMetKinderen = table.Column<bool>(nullable: false),
                    Opmerking = table.Column<string>(nullable: true),
                    RedenAfstand = table.Column<string>(nullable: false),
                    Adopteerbaar = table.Column<bool>(nullable: false),
                    GeadopteerdDoorId = table.Column<int>(nullable: true),
                    VerblijfId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dier", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Dier_Klant_GeadopteerdDoorId",
                        column: x => x.GeadopteerdDoorId,
                        principalTable: "Klant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Dier_Verblijf_VerblijfId",
                        column: x => x.VerblijfId,
                        principalTable: "Verblijf",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Behandeling",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TypeBehandeling = table.Column<string>(nullable: false),
                    Omschrijving = table.Column<string>(nullable: true),
                    Kosten = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    MagVanafLeeftijd = table.Column<int>(nullable: false),
                    NaamBehandelaar = table.Column<string>(nullable: false),
                    DatumBehandeling = table.Column<DateTime>(nullable: false),
                    DierId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Behandeling", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Behandeling_Dier_DierId",
                        column: x => x.DierId,
                        principalTable: "Dier",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Opmerking",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Tekst = table.Column<string>(nullable: false),
                    DatumOpmerking = table.Column<DateTime>(nullable: false),
                    OpmerkingGemaaktDoorId = table.Column<int>(nullable: false),
                    DierId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Opmerking", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Opmerking_Dier_DierId",
                        column: x => x.DierId,
                        principalTable: "Dier",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Opmerking_Medewerker_OpmerkingGemaaktDoorId",
                        column: x => x.OpmerkingGemaaktDoorId,
                        principalTable: "Medewerker",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Behandeling_DierId",
                table: "Behandeling",
                column: "DierId");

            migrationBuilder.CreateIndex(
                name: "IX_Dier_GeadopteerdDoorId",
                table: "Dier",
                column: "GeadopteerdDoorId");

            migrationBuilder.CreateIndex(
                name: "IX_Dier_VerblijfId",
                table: "Dier",
                column: "VerblijfId");

            migrationBuilder.CreateIndex(
                name: "IX_Opmerking_DierId",
                table: "Opmerking",
                column: "DierId");

            migrationBuilder.CreateIndex(
                name: "IX_Opmerking_OpmerkingGemaaktDoorId",
                table: "Opmerking",
                column: "OpmerkingGemaaktDoorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Behandeling");

            migrationBuilder.DropTable(
                name: "Opmerking");

            migrationBuilder.DropTable(
                name: "VacatureDier");

            migrationBuilder.DropTable(
                name: "Dier");

            migrationBuilder.DropTable(
                name: "Medewerker");

            migrationBuilder.DropTable(
                name: "Klant");

            migrationBuilder.DropTable(
                name: "Verblijf");
        }
    }
}
