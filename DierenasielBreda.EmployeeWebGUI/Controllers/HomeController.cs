﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DierenasielBreda.EmployeeWebGUI.Models;
using DierenasielBreda.Core.DomainServices;
using DierenasielBreda.EmployeeWebGUI.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace DierenasielBreda.EmployeeWebGUI.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private IDierRepository dierRepository;
        public int PageSize = 3;


        public HomeController(ILogger<HomeController> logger, IDierRepository repo)
        {
            _logger = logger;
            dierRepository = repo;
        }

        public ViewResult Index(string category, int Page = 1) => View(new DierListViewModel
        {
            Dieren = dierRepository.GetDieren()
                .Where(d => category == null || d.Ras == category)
                .OrderBy(d => d.Naam)
                .Skip((Page - 1) * PageSize)
                .Take(PageSize),

            PagingInfo = new PagingInfo
            {
                CurrentPage = Page,
                ItemsPerPage = PageSize,
                TotalItems = category == null ?
                    dierRepository.GetDieren().Count() :
                dierRepository.GetDieren().Where(e => e.Ras == category).Count()
            },
            CurrentCategory = category
        });

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
