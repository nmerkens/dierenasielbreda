﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DierenasielBreda.Core.DomainModel;
using DierenasielBreda.Core.DomainServices;
using DierenasielBreda.EmployeeWebGUI.Models.ViewModels;
using System.Threading;
using Microsoft.AspNetCore.Authorization;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace DierenasielBreda.EmployeeWebGUI.Controllers
{
    [Authorize(Roles = "Vrijwilliger, Medewerker, Admin")]
    public class DierController : Controller
    {
        private IDierRepository repository;
        private IVacatureDierRepository vRepository;
        private IVerblijfRepository verblijfRepository;
        public int PageSize = 25;

        

        public DierController(IDierRepository repo,IVacatureDierRepository vRepo, IVerblijfRepository verblijfRepo)
        {
            repository = repo;
            vRepository = vRepo;
            verblijfRepository = verblijfRepo;
        }

        [AllowAnonymous]
        public async Task<ViewResult> List(string category, int Page = 1) => View(new DierListViewModel
        {
            Dieren = await repository.GetDierenAsync(),

            PagingInfo = new PagingInfo
            {
                CurrentPage = Page,
                ItemsPerPage = PageSize,
                TotalItems = category == null ?
                    repository.GetDieren().Count() :
                repository.GetDieren().Where(e => e.Ras == category).Count()
            },
            CurrentCategory = category
        });

        [HttpGet]
        public ViewResult Add()
        {
            return View();
        }
        [HttpGet]
        public ViewResult AddWithId(int Id)
        {
            var vDier = vRepository.GetVacatureDier(Id);

            var dier = new AddDierViewModel
            {
                Naam = vDier.Naam,
                Geboortedatum = default,
                GeschatteLeeftijd = default,
                Omschrijving = null,
                Diersoort = vDier.Diersoort,
                Ras = null,
                Geslacht = vDier.Geslacht,
                Foto = null,
                DatumBinnenkomst = default,
                Gecastreerd = vDier.Gecastreerd,
                KanMetKinderen = null,
                RedenAfstand = vDier.RedenAfstand,
                Adopteerbaar = default
            };
            return View("Add", dier);
        }
        
        [HttpPost]
        public async Task<ViewResult> Add(AddDierViewModel model)
        {
            if (ModelState.IsValid)
            {

                var dier = new Dier
                {
                    Naam = model.Naam,
                    Geboortedatum = model.Geboortedatum,
                    Leeftijd = calculateAge(model.Geboortedatum),
                    GeschatteLeeftijd = model.GeschatteLeeftijd,
                    Omschrijving = model.Omschrijving,
                    Diersoort = model.Diersoort,
                    Ras = model.Ras,
                    Geslacht = model.Geslacht,
                    Foto = model.Foto,
                    DatumBinnenkomst = model.DatumBinnenkomst,
                    DatumAdoptie = null,
                    DatumOverlijden = null,
                    Gecastreerd = model.Gecastreerd,
                    KanMetKinderen = model.KanMetKinderen,
                    Opmerking = null,
                    RedenAfstand = model.RedenAfstand,
                    Adopteerbaar = model.Adopteerbaar,
                    VerblijfId = getVerblijf(model)
                   
                };

                
                if(dier.VerblijfId == 0)
                {
                    return View("Confirmation", dier);
                }

                await repository.AddDierAsync(dier);
                
                return View("Confirmation", dier);
            }
            else
            {
                return View("Add");
            }
            
        }
        [HttpGet]
        public IActionResult Detail(int Id)
        {
            Dier dier = repository.GetDier(Id);
            return View(dier);
        }
        public ViewResult Confirmation()
        {
            return View();
        }

        private int calculateAge(DateTime start)
        {
            /*
            int age = 0;
            age = DateTime.Now.Year - start.Year;
            if (DateTime.Now.DayOfYear < start.DayOfYear)
                age = age - 1;

            return age;*/

            DateTime end = DateTime.Now;
            return (end.Year - start.Year - 1) +
                (((end.Month > start.Month) ||
                ((end.Month == start.Month) && (end.Day >= start.Day))) ? 1 : 0);
        }

        private int getVerblijf(AddDierViewModel model)
        {
            var verblijven = verblijfRepository.GetVerblijven();
            if (model.Individueel == true)
            {
                foreach (Verblijf verblijf in verblijven)
                {
                    if (verblijf.Capaciteit == 1 && verblijf.Dieren == null && verblijf.Diersoort == model.Diersoort)
                    {
                        return verblijf.Id;
                    }
                }
            }
            else
            {
                foreach (Verblijf verblijf in verblijven)
                {
                    if (verblijf.Dieren == null || verblijf.Dieren.Count() < verblijf.Capaciteit)
                    {
                        if(model.Gecastreerd == true)
                        {
                            if (verblijf.Castratie == true && verblijf.Capaciteit > 1 && verblijf.Diersoort == model.Diersoort)
                            {
                                return verblijf.Id;
                            }
                        }
                        else
                        {
                            if (verblijf.Castratie == false && verblijf.Geslacht == model.Geslacht && verblijf.Capaciteit > 1 && verblijf.Diersoort == model.Diersoort)
                            {
                                return verblijf.Id;
                            }
                        }   
                    }
                }
            }
            
            return 0;
        }
    }
}
