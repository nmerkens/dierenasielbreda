﻿using DierenasielBreda.Core.DomainModel;
using DierenasielBreda.Core.DomainServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DierenasielBreda.EmployeeWebGUI.Controllers
{
    //[Authorize(Roles ="Klant")]
    public class VacatureDierController : Controller
    {
        private IVacatureDierRepository repository;
        private IVerblijfRepository verblijfRepository;
        public VacatureDierController(IVacatureDierRepository repo, IVerblijfRepository verblijfRepo)
        {
            repository = repo;
            verblijfRepository = verblijfRepo;
        }
        [Authorize(Roles = "Klant")]
        [HttpGet]
        public ViewResult Create()
        {
            ViewBag.plaatsenTotaalKat = getTotalPlaatsenKat();
            ViewBag.plaatsenTotaalHond = getTotalPlaatsenHond();
            return View();
        }
        [Authorize(Roles = "Klant")]
        [HttpPost]
        public async Task<ViewResult> Create(VacatureDier model)
        {


            if (ModelState.IsValid)
            {
                var vDier = new VacatureDier
                {
                    Naam = model.Naam,
                    Diersoort = model.Diersoort,
                    Geslacht = model.Geslacht,
                    Gecastreerd = model.Gecastreerd,
                    RedenAfstand = model.RedenAfstand,
                };

                await repository.AddVacatureDierAsync(vDier);

                return View("Confirmation", vDier);
            }
            else
            {
                return View("Add");
            }
        }
        [Authorize(Roles = "Vrijwilliger, Medewerker, Admin")]
        [HttpGet]
        public async Task<ViewResult> List()
        {
            var vDier = await repository.GetVacatureDierenAsync();
            return View(vDier);
        }

        [Authorize(Roles = "Vrijwilliger, Medewerker, Admin")]
        [HttpGet]
        public IActionResult Delete(int Id)
        {
            //ViewBag.roleId = Id;
            VacatureDier vDier = repository.GetVacatureDier(Id);
            return View(vDier);
        }
        [Authorize(Roles = "Vrijwilliger, Medewerker, Admin")]
        [HttpPost]
        public IActionResult Delete(VacatureDier vDier)
        {

            repository.DeleteVacatureDier(vDier.Id);

            return RedirectToAction("List");
        }

        private int getTotalPlaatsenHond()
        {
            int honden = 0;
            int capaciteit = 0;
            foreach(Verblijf verblijf in verblijfRepository.GetVerblijven())
            {
                if(verblijf.Diersoort == "Hond")
                {
                    capaciteit += verblijf.Capaciteit;
                    if (verblijf.Dieren != null)
                    {
                        int x = verblijf.Dieren.Count();
                        honden += x;
                    }
                }     
            }
            int result = (capaciteit - honden);
            return result;
        }
        private int getTotalPlaatsenKat()
        {
            int katten = 0;
            int capaciteit = 0;
            foreach (Verblijf verblijf in verblijfRepository.GetVerblijven())
            {
                if(verblijf.Diersoort == "Kat")
                {
                    capaciteit += verblijf.Capaciteit;
                    if (verblijf.Dieren != null)
                    {
                        katten += verblijf.Dieren.Count();
                    }
                }            
            }
            int result = (capaciteit - katten);
            return result;
        }
    }
}
