using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using DierenasielBreda.Core.DomainModel;
using DierenasielBreda.Core.DomainServices;

using DierenasielBreda.DB;
using DierenasielBreda.DBIdentity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Hosting;

namespace DierenasielBreda.EmployeeWebGUI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options =>
            {
                options.EnableEndpointRouting = false;
                var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
                options.Filters.Add(new AuthorizeFilter(policy));
            }).AddXmlSerializerFormatters();

            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(
                    Configuration["Data:DierenasielBreda:ConnectionString"]));

            //Adding identity services
            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationIdentityDbContext>();

            services.AddDbContext<ApplicationIdentityDbContext>(options => options.UseSqlServer(
                   Configuration["Data:DierenasielBredaIdentity:ConnectionString"]));

            // TODO: transient vs scoped
            services.AddTransient<IDierRepository, EFDierRepository>();
            services.AddTransient<IVacatureDierRepository, EFVacatureDierRepository>();
            services.AddTransient<IVerblijfRepository, EFVerblijfRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseStatusCodePages();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseRouting();

            app.UseMvc(routes =>
            {
                routes.MapRoute(name: "Error", template: "Error",
                    defaults: new { controller = "Error", action = "Error" });

                //Account Controller
                routes.MapRoute(
                    name: null,
                    template: "/Account/Register",
                    defaults: new { controller = "Account", action = "Register"});
                
                routes.MapRoute(
                    name: null,
                    template: "/Account/Login",
                    defaults: new { controller = "Account", action = "Login" });

                routes.MapRoute(
                    name: null,
                    template: "/Account/Logout",
                    defaults: new { controller = "Account", action = "Logout" });

                //Administration Controller
                routes.MapRoute(
                    name: null,
                    template: "/Administration/CreateRole",
                    defaults: new { controller = "Administration", action = "CreateRole" });

                routes.MapRoute(
                    name: null,
                    template: "/Administration/ListRoles",
                    defaults: new { controller = "Administration", action = "ListRoles" });

                routes.MapRoute(
                    name: null,
                    template: "/Administration/EditRole",
                    defaults: new { controller = "Administration", action = "EditRole" });

                routes.MapRoute(
                    name: null,
                    template: "/Administration/EditUsersInRole",
                    defaults: new { controller = "Administration", action = "EditUsersInRole" });

                //VacatureDier controller
                routes.MapRoute(
                    name: null,
                    template: "/VacatureDier/Create",
                    defaults: new { controller = "VacatureDier", action = "Create" });

                routes.MapRoute(
                    name: null,
                    template: "/VacatureDier/Confirmation",
                    defaults: new { controller = "VacatureDier", action = "Confirmation" });

                routes.MapRoute(
                    name: null,
                    template: "/VacatureDier/List",
                    defaults: new { controller = "VacatureDier", action = "List" });

                routes.MapRoute(
                    name: null,
                    template: "/VacatureDier/Delete/{Id:int}",
                    defaults: new { controller = "VacatureDier", action = "Delete"});

                //Dier controller
                

                routes.MapRoute(
                    name: null,
                    template: "/Dier/Add",
                    defaults: new { controller = "Dier", action = "Add"});

                routes.MapRoute(
                    name: null,
                    template: "/Dier/Add/{Id:int}",
                    defaults: new { controller = "Dier", action = "AddWithId" });

                routes.MapRoute(
                    name: null,
                    template: "/Dier/Confirmation",
                    defaults: new { controller = "Dier", action = "Confirmation" });

                routes.MapRoute(
                    name: null,
                    template: "/Dier/Detail/{Id:Int}",
                    defaults: new { controller = "Dier", action = "Detail" });
                
                routes.MapRoute(
                    name: null,
                    template: "/",
                    defaults: new { controller = "Dier", action = "List", Page = 1 });

            });
        }
    }
}