﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using FoolProof.Core;

namespace DierenasielBreda.EmployeeWebGUI.Models.ViewModels
{
    public class AddDierViewModel
    {
        public string Naam { get; set; }
        [RequiredIfFalse("GeschatteLeeftijd")]
        public DateTime Geboortedatum { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een leeftijd in")]

        [RequiredIfFalse("Geboortedatum")]
        public int GeschatteLeeftijd { get; set; }
        [Required(ErrorMessage = "vul alstublieft een omschrijving in")]
        public string Omschrijving { get; set; }
        [Required(ErrorMessage = "vul alstublieft een Diersoort in")]
        public string Diersoort { get; set; }
        public string Ras { get; set; }
        [Required(ErrorMessage = "vul alstublieft een Geslacht in")]
        public bool Geslacht { get; set; }
        public string Foto { get; set; }
        [Required(ErrorMessage = "vul alstublieft een DatumBinnenmokst in")]
        public DateTime DatumBinnenkomst { get; set; }
        [Required(ErrorMessage = "vul alstublieft in of dier gecastreerd is")]
        public bool Gecastreerd { get; set; }
        [Required(ErrorMessage = "vul alstublieft of dier met kinderen kan omgaan")]
        public bool? KanMetKinderen { get; set; }
        [Required(ErrorMessage = "vul alstublieft een redenen van afstand in")]
        public string RedenAfstand { get; set; }
        [Required(ErrorMessage = "vul alstublieft in of dier adopteerbaar is")]
        public bool Adopteerbaar { get; set; }

        public bool Individueel { get; set; }

    }
}
