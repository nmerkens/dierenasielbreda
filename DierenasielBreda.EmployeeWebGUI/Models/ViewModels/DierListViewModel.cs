﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DierenasielBreda.Core.DomainModel;

namespace DierenasielBreda.EmployeeWebGUI.Models.ViewModels
{
    public class DierListViewModel
    {
        public IEnumerable<Dier> Dieren { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public string CurrentCategory { get; set; }

    }
}
