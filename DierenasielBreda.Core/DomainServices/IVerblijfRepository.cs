﻿using DierenasielBreda.Core.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DierenasielBreda.Core.DomainServices
{
    public interface IVerblijfRepository
    {
        Verblijf GetVerblijf(int verblijfId);

        // Lijst met dieren opvragen
        IQueryable<Verblijf> GetVerblijven();
        Task<IEnumerable<Verblijf>> GetverblijvenAsync();
    }
}
