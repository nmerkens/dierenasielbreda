﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DierenasielBreda.Core.DomainModel;

namespace DierenasielBreda.Core.DomainServices
{
    public interface IDierRepository
    {
        // TODO: Aanvullende functies zoals: Dier toevoegen aan klant als adoptie

        // zoek specifiek dier
        Dier GetDier(int dierId);

        // Lijst met dieren opvragen
        IQueryable<Dier> GetDieren();
        Task<IEnumerable<Dier>> GetDierenAsync();

        // Dier opslaan
        Dier AddDier(Dier dier);
        Task<Dier> AddDierAsync(Dier dier);

        // Dier updaten
        void UpdateDier(Dier dier);

        // Dier verwijderen, returned verwijderd object(dier)
        Dier DeleteDier(int dierId);
    }
}
