﻿using System;
using System.Linq;

using DierenasielBreda.Core.DomainModel;

namespace DierenasielBreda.Core.DomainServices
{
    public interface IKlantRepository
    {
        // functies van/op klant

        // Klant toevoegen
        Klant AddKlant(Klant klant);

        // Klant updaten
        void UpdateKlant(Klant klant);

        // Klant verwijderen
        Klant DeleteKlant(int klantId);

        // Klant opvragen(met Id)
        Klant GetKlant(int klantId);

        // Alle klanten opvragen
        IQueryable<Klant> GetAllKlanten();




    }
}
