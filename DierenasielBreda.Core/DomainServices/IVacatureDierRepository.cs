﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using DierenasielBreda.Core.DomainModel;

namespace DierenasielBreda.Core.DomainServices
{
    public interface IVacatureDierRepository
    {
        VacatureDier GetVacatureDier(int vDierId);

        // Lijst met dieren opvragen
        IQueryable<VacatureDier> GetVacatureDieren();
        Task<IEnumerable<VacatureDier>> GetVacatureDierenAsync();

        // Dier opslaan
        Task<VacatureDier> AddVacatureDierAsync(VacatureDier vDier);

        // Dier updaten
        void UpdateDier(VacatureDier vDier);

        // Dier verwijderen, returned verwijderd object(dier)
        VacatureDier DeleteVacatureDier(int vDierId);
    }
}
