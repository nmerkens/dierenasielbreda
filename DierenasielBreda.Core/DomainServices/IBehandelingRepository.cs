﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using DierenasielBreda.Core.DomainModel;

namespace DierenasielBreda.Core.DomainServices
{
    public interface IBehandelingRepository
    {
        // zoek specifiek Behandeling
        Behandeling GetBehandeling(int behandelingId);

        // Lijst met Behandelingen opvragen
        IQueryable<Behandeling> GetAllBehandeling();

        // Behadeling toevoegen
        Behandeling AddBehandeling(Behandeling behandeling);

        // Behandeling aanpassen
        void UpdateBehandeling(Behandeling behandeling);

        // Behandeling verwijderen
        void DeleteBehandeling(int behandelingId);
    }
}
