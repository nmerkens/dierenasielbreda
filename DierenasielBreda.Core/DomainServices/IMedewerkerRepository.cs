﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DierenasielBreda.Core.DomainModel;

namespace DierenasielBreda.Core.DomainServices
{
    public interface IMedewerkerRepository
    {
        // zoek specifieke medewerker
        Medewerker GetMedewerker(int medewerkerId);

        // Lijst met medewerkers opvragen
        IQueryable<Medewerker> GetAllMedewerker();

        // Medewerker toevoegen
        Medewerker AddMedewerker(Medewerker medewerker);

        // medewerker aanpassen
        void UpdateMedewerker(Medewerker medewerker);

        // medewerker verwijderen
        void DeleteMedewerker(int medewerkerId);
    }
}
