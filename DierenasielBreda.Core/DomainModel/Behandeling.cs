﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DierenasielBreda.Core.DomainModel
{
    public class Behandeling
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een naam in")]
        public string TypeBehandeling { get; set; }
        public string Omschrijving { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Kosten { get; set; }
        public int MagVanafLeeftijd { get; set; }
        [Required(ErrorMessage = "Vul alstublieft naam van behandelaar in")]
        public string NaamBehandelaar { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een datum van behandeling in")]
        public DateTime DatumBehandeling { get; set; }

        //relaties
        public Dier Dier { get; set; }
    }
}
