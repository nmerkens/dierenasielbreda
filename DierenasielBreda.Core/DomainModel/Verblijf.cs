﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DierenasielBreda.Core.DomainModel
{
    public class Verblijf
    {
        public int Id { get; set; }
        public string Naam { get; set; }
        public int Capaciteit { get; set; }
        public string Diersoort { get; set; }
        public bool? Geslacht { get; set; }
        public bool? Castratie { get; set; }

        //relaties
        public List<Dier> Dieren { get; set; }
    }
}
