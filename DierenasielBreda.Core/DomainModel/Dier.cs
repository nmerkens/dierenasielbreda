﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using FoolProof.Core;
//using Microsoft.AspNetCore.Mvc.ModelBinding;



namespace DierenasielBreda.Core.DomainModel
{
    public class Dier
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een naam in")]
        public string Naam { get; set; }
        [RequiredIfFalse("GeschatteLeeftijd")]
        public DateTime Geboortedatum { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een leeftijd in")]
        public int Leeftijd { get; set; }
        [RequiredIfFalse("Geboortedatum")]
        public int GeschatteLeeftijd { get; set; }
        [Required(ErrorMessage = "vul alstublieft een omschrijving in")]
        public string Omschrijving { get; set; }
        [Required(ErrorMessage = "vul alstublieft een Diersoort in")]
        public string Diersoort { get; set; }
        public string Ras { get; set; }
        [Required(ErrorMessage = "vul alstublieft een Geslacht in")]
        public bool Geslacht { get; set; }
        public string Foto { get; set; }
        [Required(ErrorMessage = "vul alstublieft een DatumBinnenmokst in")]
        public DateTime DatumBinnenkomst { get; set; }
        public DateTime? DatumAdoptie { get; set; }
        public DateTime? DatumOverlijden { get; set; }
        [Required(ErrorMessage = "vul alstublieft in of dier gecastreerd is")]
        public bool Gecastreerd { get; set; }
        [Required(ErrorMessage = "vul alstublieft of dier met kinderen kan omgaan")]
        public bool? KanMetKinderen { get; set; }
        public string Opmerking { get; set; }
        [Required(ErrorMessage = "vul alstublieft een redenen van afstand in")]
        public string RedenAfstand { get; set; }
        [Required(ErrorMessage = "vul alstublieft in of dier adopteerbaar is")]
        public bool Adopteerbaar { get; set; }

        //Relaties 

        //veel op 1
        public List<Opmerking> Opmerkingen { get; set; }
        public List<Behandeling> Behandelingen { get; set; }

        //1 op veel
        public Klant GeadopteerdDoor { get; set; }
        public int VerblijfId { get; set; }
        public Verblijf Verblijf { get; set; }
    }
}
