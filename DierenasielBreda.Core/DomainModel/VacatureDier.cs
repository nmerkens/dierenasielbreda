﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using FoolProof;

namespace DierenasielBreda.Core.DomainModel
{
    public class VacatureDier
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een naam in")]
        public string Naam { get; set; }
        [Required(ErrorMessage = "Vul alstublieft in welke diersoort")]
        public string Diersoort { get; set; }
        [Required(ErrorMessage = "Vul alstublieft in welk geslacht het dier is")]
        public bool Geslacht { get; set; }
        [Required(ErrorMessage = "Vul alstublieft in of dier gecastreerd is")]
        public bool Gecastreerd { get; set; }
        [Required(ErrorMessage = "Vul alstublieft Reden van afstand in")]
        public string RedenAfstand { get; set; }
    }
}
