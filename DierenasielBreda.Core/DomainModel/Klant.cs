﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DierenasielBreda.Core.DomainModel
{
    public class Klant
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een voornaam in")]
        public string Voornaam { get; set; }
        public string Tussenvoegsel { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een achternaam in")]
        public string Achternaam { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een emailadres in")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een Telefoonnummer in")]
        public int TelefoonNummer { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een GeboorteDatum in")]
        public DateTime GeboorteDatum { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een Straatnaam in")]
        public string Straatnaam { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een huisnummer in")]
        public int Huisnummer { get; set; }
        public string HuisnummerToevoeging { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een postcode in")]
        public string Postcode { get; set; }
        public string Plaatsnaam { get; set; }


        //relaties
        //veel op 1
        public List<Dier> Dieren { get; set; }
    }
}
