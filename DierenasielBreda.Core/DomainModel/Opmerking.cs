﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DierenasielBreda.Core.DomainModel
{
    public class Opmerking
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een Opmerking in")]
        public string Tekst { get; set; }
        //moet automatisch gebueren (get.Date())
        [Required(ErrorMessage = "Vul alstublieft een DatumOpmerking in")]
        public DateTime DatumOpmerking { get; set; }


        //relaties
        //moet ook automatisch gebeuren (medewerker logt in om opmerking te maken)
        [Required(ErrorMessage = "Vul alstublieft in wie de opmerking heeft gemaakt")]
        public Medewerker OpmerkingGemaaktDoor { get; set; }

        public Dier Dier { get; set; }

    }
}
