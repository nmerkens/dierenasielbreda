﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace DierenasielBreda.Core.DomainModel
{
    public class Medewerker 
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een voornaam in")]
        public string Voornaam { get; set; }
        public string Tussenvoegsel { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een achternaam in")]
        public string Achternaam { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een emailadres in")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een Telefoonnummer in")]
        public int TelefoonNummer { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een GeboorteDatum in")]
        public DateTime GeboorteDatum { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een Wachtwoord in")]
        public string Wachtwoord { get; set; }
        [Required(ErrorMessage = "Vul alstublieft een in of persoon een vrijwilliger is")]
        public bool IsVrijwilliger { get; set; }

        //relaties
        //veel op 1
        public int OpmerkingID { get; set; }
        public List<Opmerking> Opmerkingen { get; set; }
    }
}
